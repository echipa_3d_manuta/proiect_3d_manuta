#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include <glew.h>

#include <GLM.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

#include <glfw3.h>

#pragma comment (lib, "x64/glfw3dll.lib")
#pragma comment (lib, "x64/glew32.lib")
#pragma comment (lib, "OpenGL32.lib")

#include "Camera.h"
#include "Shader.h"
#include "Light.h"
#include "Model.h"
#include "Particle.h"
#include "Skybox.h"

// settings
const unsigned int SCR_WIDTH = 1280;
const unsigned int SCR_HEIGHT = 720;

GLuint ProjMatrixLocation, ViewMatrixLocation, WorldMatrixLocation;
Camera *pCamera = nullptr;

void Cleanup()
{
	delete pCamera;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow *window);

// timing
int timerFrame = 0;
double deltaTime = 0.0f;    // time between current frame and last frame
double lastFrame = 0.0f;

bool lightPositionFlag = false;
glm::vec3 controlledLightPosition = glm::vec3(0.0f);

ParticleGenerator *Particles;

int main()
{
	// glfw: initialize and configure
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_SAMPLES, 4);

	// glfw window creation
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Proiect Manuta", NULL, NULL);
	if (window == NULL) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	glewInit();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_MULTISAMPLE);
	/*glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);*/

	// Create camera
	pCamera = new Camera(SCR_WIDTH, SCR_HEIGHT, glm::vec3(0.0, 0.0, 3.0));


	Model hands("models/hand/hands.obj"), painting("models/painting/fbxPainting.fbx");
	Model leftHand("models/hand/left_hand.obj"), rightHand("models/hand/right_hand.obj");
	Shader shader("shaders/vs/Shader.vs", "shaders/fs/Shader.fs");
	Shader particleShader("shaders/vs/Particle.vs", "shaders/fs/Particle.fs");
	Shader shadowShader("shaders/vs/ShadowDepth.vs", "shaders/fs/ShadowDepth.fs", "shaders/gs/ShadowDepth.gs");
	Shader lampShader("shaders/vs/Lamp.vs", "shaders/fs/Lamp.fs");
	Shader skyboxShader("shaders/vs/SkyBox.vs", "shaders/fs/SkyBox.fs");
	Skybox spaceSkybox("models/skybox/");
	Light light;

	ParticleGenerator particles(particleShader, Texture::textureFromFile("particle.png", "models/particles/texture"), 0);

	glm::vec3 handsPosition = { 0.0f, -1.0f, 0.0f };
	glm::vec3 leftHandPosition = { 0.0f, -1.0f, 0.0f };
	glm::vec3 rightHandPosition = { 0.0f, -1.0f, 0.0f };
	glm::vec3 firstPaintingPosition = { 0.0f, -3.35f, -5.8f };
	glm::vec3 secondPaintingPosition = { -4.0f, -3.35f, -3.0f };
	glm::vec3 thirdPaintingPosition = { 4.0f, -3.35f, -3.0f };
	glm::vec3 lightInbetweenHandsPosition = { 0.0f, 0.0f, 0.0f };

	// Point shadow initialization
	unsigned int depthCubemap;
	glGenTextures(1, &depthCubemap);
	unsigned int depthMapFBO;
	glGenFramebuffers(1, &depthMapFBO);

	const unsigned int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;
	glBindTexture(GL_TEXTURE_CUBE_MAP, depthCubemap);
	for (unsigned int i = 0; i < 6; ++i)
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthCubemap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	light.lightPosition = glm::vec3(0.0f, 0.0f, 0.0f);

	float lightPositionAngle = 0.0f;
	float lightPositionRadius = 2.0f;

	// render loop
	while (!glfwWindowShouldClose(window)) {
		// per-frame time logic
		double currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;
		if (timerFrame > 0)
			timerFrame -= 1;

		// input
		processInput(window);

		lightPositionAngle = lightPositionAngle + 10 * deltaTime;

		if (lightPositionFlag == true)
			light.lightPosition = controlledLightPosition;
		else {
			light.lightPosition.x = glm::cos(glm::radians(lightPositionAngle)) * lightPositionRadius;
			light.lightPosition.z = glm::sin(glm::radians(lightPositionAngle)) * lightPositionRadius;
		}

		// rendering
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		float near_plane = 1.0f;
		float far_plane = 25.0f;
		glm::mat4 model = glm::mat4(1.0f);
		glm::mat4 shadowProj = glm::perspective(glm::radians(90.0f), (float)SHADOW_WIDTH / (float)SHADOW_HEIGHT, near_plane, far_plane);
		std::vector<glm::mat4> shadowTransforms;
		shadowTransforms.push_back(shadowProj * glm::lookAt(light.lightPosition, light.lightPosition + glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
		shadowTransforms.push_back(shadowProj * glm::lookAt(light.lightPosition, light.lightPosition + glm::vec3(-1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
		shadowTransforms.push_back(shadowProj * glm::lookAt(light.lightPosition, light.lightPosition + glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)));
		shadowTransforms.push_back(shadowProj * glm::lookAt(light.lightPosition, light.lightPosition + glm::vec3(0.0f, -1.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f)));
		shadowTransforms.push_back(shadowProj * glm::lookAt(light.lightPosition, light.lightPosition + glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
		shadowTransforms.push_back(shadowProj * glm::lookAt(light.lightPosition, light.lightPosition + glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, -1.0f, 0.0f)));

		// 1. render scene to depth cubemap
		// --------------------------------
		glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
		glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
		glClear(GL_DEPTH_BUFFER_BIT);
		shadowShader.use();
		for (unsigned int i = 0; i < 6; ++i)
			shadowShader.setMat4("shadowMatrices[" + std::to_string(i) + "]", shadowTransforms[i]);
		shadowShader.setFloat("far_plane", far_plane);
		shadowShader.setVec3("lightPos", light.lightPosition);

		model = glm::mat4(1.0f);
		model = glm::translate(model, firstPaintingPosition);
		shadowShader.setMat4("model", model);
		painting.draw(shadowShader);

		model = glm::mat4(1.0f);
		model = glm::translate(model, secondPaintingPosition);
		model = glm::rotate(model, glm::radians(70.0f), { 0.0f, 1.0f, 0.0f });
		shadowShader.setMat4("model", model);
		painting.draw(shadowShader);

		model = glm::mat4(1.0f);
		model = glm::translate(model, thirdPaintingPosition);
		model = glm::rotate(model, glm::radians(-70.0f), { 0.0f, 1.0f, 0.0f });
		shadowShader.setMat4("model", model);
		painting.draw(shadowShader);

		/*model = glm::mat4(1.0f);
		model = glm::translate(model, leftHandPosition);
		model = glm::scale(model, { 0.2f, 0.2f, 0.2f });
		shadowShader.setMat4("model", model);
		leftHand.draw(shadowShader);

		model = glm::mat4(1.0f);
		model = glm::translate(model, rightHandPosition);
		model = glm::scale(model, { 0.2f, 0.2f, 0.2f });
		shadowShader.setMat4("model", model);
		rightHand.draw(shadowShader);*/

		model = glm::mat4(1.0f);
		model = glm::translate(model, handsPosition);
		model = glm::scale(model, { 0.2f, 0.2f, 0.2f });
		shadowShader.setMat4("model", model);
		hands.draw(shadowShader);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		// 2. render scene as normal
		// -------------------------
		glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glm::mat4 projection = pCamera->getProjectionMatrix();;
		glm::mat4 view = glm::mat4(glm::mat3(pCamera->getViewMatrix()));

		spaceSkybox.draw(skyboxShader, projection, view);

		view = pCamera->getViewMatrix();

		shader.use();
		shader.setMat4("projection", projection);
		shader.setMat4("view", view);
		// set lighting uniforms
		shader.setVec3("lightPos", light.lightPosition);
		shader.setVec3("lightColor", light.lightColor);
		shader.setVec3("viewPos", pCamera->getPosition());
		shader.setInt("shadows", 1); // enable/disable shadows by pressing 'SPACE'
		shader.setFloat("far_plane", far_plane);
		shader.setInt("depthMap", 1);
		shader.setInt("reverse_normals", 0);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_CUBE_MAP, depthCubemap);

		model = glm::mat4(1.0f);
		model = glm::translate(model, firstPaintingPosition);
		shader.setMat4("model", model);
		painting.draw(shader);

		model = glm::mat4(1.0f);
		model = glm::translate(model, secondPaintingPosition);
		model = glm::rotate(model, glm::radians(70.0f), { 0.0f, 1.0f, 0.0f });
		shader.setMat4("model", model);
		painting.draw(shader);

		model = glm::mat4(1.0f);
		model = glm::translate(model, thirdPaintingPosition);
		model = glm::rotate(model, glm::radians(-70.0f), { 0.0f, 1.0f, 0.0f });
		shader.setMat4("model", model);
		painting.draw(shader);

		/*model = glm::mat4(1.0f);
		model = glm::translate(model, leftHandPosition);
		model = glm::scale(model, { 0.2f, 0.2f, 0.2f });
		shader.setMat4("model", model);
		leftHand.draw(shader);

		model = glm::mat4(1.0f);
		model = glm::translate(model, rightHandPosition);
		model = glm::scale(model, { 0.2f, 0.2f, 0.2f });
		shader.setMat4("model", model);
		rightHand.draw(shader);*/

		model = glm::mat4(1.0f);
		model = glm::translate(model, handsPosition);
		model = glm::scale(model, { 0.2f, 0.2f, 0.2f });
		shader.setMat4("model", model);
		hands.draw(shader);

		lampShader.use();
		lampShader.setMat4("projection", projection);
		lampShader.setMat4("view", view);
		model = glm::mat4(1.0f);
		model = glm::translate(model, light.lightPosition);
		lampShader.setMat4("model", model);
		light.draw(lampShader);

		particleShader.use();
		particleShader.setMat4("projection", projection);
		particleShader.setMat4("view", view);

		particles.Update(deltaTime, &(light.lightPosition), 10);
		particles.Draw(projection, view);

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	Cleanup();

	// glfw: terminate, clearing all previously allocated GLFW resources
	glfwTerminate();
	return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS && lightPositionFlag == true)
		controlledLightPosition.z -= 1 * deltaTime;
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS && lightPositionFlag == true)
		controlledLightPosition.z += 1 * deltaTime;
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS && lightPositionFlag == true)
		controlledLightPosition.x -= 1 * deltaTime;
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS && lightPositionFlag == true)
		controlledLightPosition.x += 1 * deltaTime;
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS && lightPositionFlag == true)
		controlledLightPosition.y += 1 * deltaTime;
	if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS && lightPositionFlag == true)
		controlledLightPosition.y -= 1 * deltaTime;

	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
		int width, height;
		glfwGetWindowSize(window, &width, &height);
		pCamera->reset(width, height);
	}

	if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS && timerFrame == 0) {
		lightPositionFlag = !lightPositionFlag;
		timerFrame = 10;
	}
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and
	// height will be significantly larger than specified on retina displays.
	pCamera->reshape(width, height);
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	pCamera->mouseControl((float)xpos, (float)ypos);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yOffset)
{
	pCamera->processMouseScroll((float)yOffset);
}