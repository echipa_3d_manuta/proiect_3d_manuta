#pragma once

#include <string>
#include <vector>

#include "Shader.h"

class Skybox {
public:
	Skybox(const std::string &filePath);
	~Skybox() = default;

	void draw(Shader &shader, glm::mat4 &projection, glm::mat4 &view);
private:
	static unsigned int loadCubemap(std::vector<std::string> faces);

	static float skyboxVertices[];
	unsigned int VAO;
	unsigned int VBO;
	unsigned int texture;
};