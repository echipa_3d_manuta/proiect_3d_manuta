#pragma once

#include <vector>
#include <string>

#include <assimp/scene.h>

#include "Mesh.h"
#include "Shader.h"
#include "Texture.h"

class Model {
public:
	std::vector<Texture> textures_loaded;

	Model(const char *path){
		loadModel(path);
	}

	void draw(Shader &shader);

private:
	std::vector<Mesh> meshes;
	std::string directory;

	void loadModel(const std::string &path);
	void processNode(aiNode *node, const aiScene *scene);
	Mesh processMesh(aiMesh *mesh, const aiScene *scene);
	std::vector<Texture> loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName);
};