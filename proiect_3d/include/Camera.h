#pragma once

#include <glm.hpp>

enum ECameraMovementType
{
	UNKNOWN,
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT,
	UP,
	DOWN
};

class Camera
{
private:
	// Default camera values
	const float zNEAR = 0.1f;
	const float zFAR = 500.f;
	const float YAW = -90.0f;
	const float PITCH = 0.0f;
	const float FOV = 45.0f;
	glm::vec3 startPosition;

public:
	glm::vec3 position;
	glm::vec3 forward;
	glm::vec3 right;
	glm::vec3 up;
	glm::vec3 worldUp;

	Camera(const int width, const int height, const glm::vec3 &position);

	void set(const int width, const int height, const glm::vec3 &position);
	void reset(const int width, const int height);
	void reshape(int windowWidth, int windowHeight);

	const glm::mat4 getViewMatrix() const;
	const glm::vec3 getPosition() const;
	const glm::mat4 getProjectionMatrix() const;

	void processKeyboard(ECameraMovementType direction, float deltaTime);
	void mouseControl(float xPos, float yPos);
	void processMouseScroll(float yOffset);

private:
	void processMouseMovement(float xOffset, float yOffset, bool constrainPitch = true);
	void updateCameraVectors();

protected:
	const float cameraSpeedFactor = 2.5f;
	const float mouseSensitivity = 0.1f;

	// Perspective properties
	float zNear;
	float zFar;
	float FoVy;
	int width;
	int height;
	bool isPerspective;

	// Euler Angles
	float yaw;
	float pitch;

	bool bFirstMouseMove = true;
	float lastX = 0.f, lastY = 0.f;
};