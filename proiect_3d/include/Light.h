#pragma once

#include <glm.hpp>

#include "Shader.h"

class Light {
public:
	Light();

	void draw(Shader shader);

	glm::vec3 lightColor;
	glm::vec3 lightPosition;

private:
	unsigned int VBO, VAO;
};