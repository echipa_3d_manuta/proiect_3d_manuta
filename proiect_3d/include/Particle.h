#pragma once

#include <GL/glew.h>
#include <glm.hpp>
#include <glew.h>
#include <vector>

#include "Model.h"

// Represents a single particle and its state
struct Particle {
	glm::vec3 Position, Velocity, Offset;
	glm::vec4 Color;
	float height = 0.0f;
	GLfloat Life;

	Particle() : Position(0.0f), Velocity(0.0f), Offset(0.0f), Color(1.0f), Life(0.0f) { }
};


// ParticleGenerator acts as a container for rendering a large number of 
// particles by repeatedly spawning and updating particles and killing 
// them after a given amount of time.
class ParticleGenerator
{
public:
	ParticleGenerator(Shader &shader, unsigned int texture, GLuint amount);

	void Update(GLfloat dt, glm::vec3 *position, GLuint newParticles, glm::vec3 offset = glm::vec3(0.0f));
	void Draw(glm::mat4 &projection, glm::mat4 &view);
private:
	std::vector<Particle> particles;

	GLuint amount;
	Shader *shader;
	GLuint VAO;
	unsigned int texture;
	void init();
};
