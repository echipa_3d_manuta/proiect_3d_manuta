#pragma once

#include <string>
#include <iostream>

class Texture {
public:
	unsigned int id;
	std::string type;
	std::string path;

	static unsigned int textureFromFile(const char *path, const std::string &directory);
};
