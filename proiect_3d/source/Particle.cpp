#include <gtc/matrix_transform.hpp>

#include "Particle.h"

ParticleGenerator::ParticleGenerator(Shader &shader, unsigned int texture, GLuint amount)
	: shader(&shader), texture(texture), amount(amount)
{
	this->init();
}

void ParticleGenerator::Update(GLfloat dt, glm::vec3 *position, GLuint newParticles, glm::vec3 offset)
{
	// Add new particles 
	/*for (GLuint i = 0; i < newParticles; ++i)
	{
		int unusedParticle = this->firstUnusedParticle();
		this->respawnParticle(this->particles[unusedParticle], position, offset);
	}*/
	for (int i = 0; i < newParticles; ++i) {
		particles.push_back(Particle());
		GLfloat random = (rand() % 100) / 1.0f, ray = 0.1f;
		float firstAngle = rand() % 360, secondAngle = rand() % 360;
		GLfloat rColor = 0.5 + ((rand() % 100) / 100.0f);
		(particles.end() - 1)->Offset = glm::vec3(ray * glm::sin(glm::radians(firstAngle)) * glm::cos(glm::radians(secondAngle)) - 0.05f,
												  ray * glm::sin(glm::radians(firstAngle)) * glm::sin(glm::radians(secondAngle)) - 0.05f,
												  ray * glm::cos(glm::radians(secondAngle)) + 0.05f);
		(particles.end() - 1)->Color = glm::vec4(rColor, rColor, rColor, 1.0f);
		(particles.end() - 1)->Life = 0.1f;
		(particles.end() - 1)->Velocity = glm::vec3(0.2f);
	}
	// Update all particles
	int a;
	for (GLuint i = 0; i < particles.size(); ++i)
	{
		particles[i].Life -= dt / 10.0f; // reduce life
		if (particles[i].Life > 0.0f)
		{	// particle is alive, thus update
			particles[i].height += 0.01;
			GLfloat random = (rand() % 10) / 1.0f;
			float pointInLife = particles[i].Life * 10.0f;
			particles[i].Position = (*position + particles[i].Offset * pointInLife) + glm::vec3{ 0.0f, particles[i].height, 0.0f };
			particles[i].Velocity *= 0.1f;
			particles[i].Color.a -= dt / 100.0f;
		}
		else
		{
			particles.erase(particles.begin() + i);
			--i;
		}
	}
}

// Render all particles
void ParticleGenerator::Draw(glm::mat4 &projection, glm::mat4 &view)
{
	// Use additive blending to give it a 'glow' effect
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glEnable(GL_TEXTURE_2D);
	this->shader->use();
	this->shader->setMat4("projection", projection);
	this->shader->setMat4("view", view);
	for (Particle &particle : this->particles)
	{
		if (particle.Life > 0.0f) {
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, particle.Position);
			model = glm::scale(model, { 0.1f, 0.1f, 0.1f });
			this->shader->setMat4("model", model);
			this->shader->setVec4("color", particle.Color);
			this->shader->setFloat("sprite", texture);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, texture);
			glBindVertexArray(this->VAO);
			glDrawArrays(GL_TRIANGLES, 0, 6);
			glBindVertexArray(0);
		}
	}
	// Don't forget to reset to default blending mode
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void ParticleGenerator::init()
{
	// Set up mesh and attribute properties
	GLuint VBO;
	GLfloat particle_quad[] = {
		0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f, 0.0f,

		0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
		1.0f, 1.0f, 0.0f, 1.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f, 0.0f
	};
	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(this->VAO);
	// Fill mesh buffer
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(particle_quad), particle_quad, GL_STATIC_DRAW);
	// Set mesh attributes
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	glBindVertexArray(0);

	// Create this->amount default particle instances
	for (GLuint i = 0; i < this->amount; ++i) {
		this->particles.push_back(Particle());
		(particles.end() - 1)->Life = 2.0f;
	}
}