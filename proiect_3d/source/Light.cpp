#include <glew.h>
#include <gtc/matrix_transform.hpp>

#include "Light.h"

Light::Light() {
	float vertices[] = {
	  -0.02f, -0.02f, -0.02f,
	   0.02f, -0.02f, -0.02f,
	   0.02f,  0.02f, -0.02f,
	   0.02f,  0.02f, -0.02f,
	  -0.02f,  0.02f, -0.02f,
	  -0.02f, -0.02f, -0.02f,

	  -0.02f, -0.02f,  0.02f,
	   0.02f, -0.02f,  0.02f,
	   0.02f,  0.02f,  0.02f,
	   0.02f,  0.02f,  0.02f,
	  -0.02f,  0.02f,  0.02f,
	  -0.02f, -0.02f,  0.02f,

	  -0.02f,  0.02f,  0.02f,
	  -0.02f,  0.02f, -0.02f,
	  -0.02f, -0.02f, -0.02f,
	  -0.02f, -0.02f, -0.02f,
	  -0.02f, -0.02f,  0.02f,
	  -0.02f,  0.02f,  0.02f,

	   0.02f,  0.02f,  0.02f,
	   0.02f,  0.02f, -0.02f,
	   0.02f, -0.02f, -0.02f,
	   0.02f, -0.02f, -0.02f,
	   0.02f, -0.02f,  0.02f,
	   0.02f,  0.02f,  0.02f,

	  -0.02f, -0.02f, -0.02f,
	   0.02f, -0.02f, -0.02f,
	   0.02f, -0.02f,  0.02f,
	   0.02f, -0.02f,  0.02f,
	  -0.02f, -0.02f,  0.02f,
	  -0.02f, -0.02f, -0.02f,

	  -0.02f,  0.02f, -0.02f,
	   0.02f,  0.02f, -0.02f,
	   0.02f,  0.02f,  0.02f,
	   0.02f,  0.02f,  0.02f,
	  -0.02f,  0.02f,  0.02f,
	  -0.02f,  0.02f, -0.02f
	};

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindVertexArray(VAO);

	// position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	lightColor = { 0.3f, 0.3f, 0.5f };
	lightPosition = glm::vec3(0.0f);
}

void Light::draw(Shader shader) {
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}
